# -*- coding: utf-8 -*-
#! /usr/local/bin/python2.7
# TP1 - Base de Datos - 2014
# 
# Nombre del campo: longitud
# Estado:1 - Posicion: 4 - Apellido: 20 - Nombre: 20 - codigoCliente: 5 
# Tamanio total de registo = 50
#
import os

long_estado = 1
long_posicion = 4
long_apellido = 20
long_nombre = 20
long_codigo = 5
long_registro = long_estado + long_posicion + long_apellido + long_nombre + long_codigo
nombre_archivo = "agenda_uno.dat"
cant_registros = 60

class Cliente:
	def __init__(self,estado, posicion,apellido,nombre,codigo):
		self.estado = estado 
		self.posicion = posicion
		self.nombre = nombre
		self.apellido = apellido
		self.codigo = codigo


def leer_registro(posicion):
	'''Leer un registro y retornar un objeto cliente'''
	archivo = open(nombre_archivo,'r+')
	archivo.seek(posicion*long_registro)
	estado = int(archivo.read(long_estado))
	posicion = int(archivo.read(long_posicion))
	apellido = archivo.read(long_apellido)
	nombre = archivo.read(long_nombre)
	codigo = archivo.read(long_codigo)
	cliente = Cliente(estado, posicion,apellido,nombre,codigo)
	archivo.close()
	return cliente

def guardar_registro(cliente):
	archivo = open(nombre_archivo, 'r+')
	archivo.seek(int(cliente.posicion) * long_registro)
	archivo.write(cliente.estado+cliente.posicion+cliente.apellido+cliente.nombre+cliente.codigo)
	archivo.close()
	return True

def imprimir_registro(cliente):
	print "Apellido: ", cliente.apellido
	print "Nombre: ", cliente.nombre
	print "Codigo de Cliente: " , cliente.codigo
	print "Posicion: ", cliente.posicion

def imprimir_listado():
	print "------ Listado de Clientes------------"
	print ''
	archivo = open(nombre_archivo,'r')
	cont = 0
	print '|',"pos".ljust(long_posicion,' '),'|',"Apellido".ljust(long_apellido,' '), '|',"Nombre".ljust(long_nombre,' '),'|',"Cod".ljust(long_codigo,' '),'|' 
	for i in range(cant_registros):
		estado = archivo.read(long_estado)
		posicion = archivo.read(long_posicion)
		apellido = archivo.read(long_apellido)
		nombre = archivo.read(long_nombre)
		codigo = archivo.read(long_codigo)
		if estado == "1":
			cont = cont +1
			print '|',posicion,'|',apellido, '|',nombre,'|',codigo,'|'
	print ''
	print("Cantidad de Registros Cargados:"), cont
	print ''
	archivo.close()
	return True

def  listar_registros():
	imprimir_listado()
	raw_input('presione una tecla para continuar....')
	return True


def agregar_registro():
	#imprimir_listado()
	print "------Agregar Cliente--------"
	print ''
	pos = int(raw_input('Ingrese el regisitro: '))
	if pos>cant_registros:
		print 'Error: el numero de registro supera la cantidad permitida. [Cantidad MAX:',cant_registros,']'
		raw_input('presione una tecla para continuar....')
		return False
	cliente = leer_registro(pos)
	#ver si el registro ya esta ocupado
	if cliente.estado == 1 :
		print "El registro esta ocupado por el cliente:"
		imprimir_registro(cliente)
		remplazar = "a"
		while remplazar != "s": 
			remplazar = raw_input('Desea remplazar el registro por uno nuevo? [n/s]: ')
			remplazar = remplazar.lower()
			if remplazar == "n":
				return False
	print "Ingresar los datos del nuevo cliente: "
	cliente.estado = "1"
	cliente.posicion = (str(cliente.posicion)).rjust(long_posicion,'0')
	cliente.apellido = (raw_input('Apellido: ')).ljust(long_apellido,' ')
	cliente.nombre = (raw_input('Nombre: ')).ljust(long_nombre,' ')
	cliente.codigo = (raw_input('Codigo: (numero de 5 digitos): ')).rjust(long_codigo,'0')
	if guardar_registro(cliente):
		print "Registro cargado existosamente"
	else:
		print "Error al intentar guardar el registro "
	raw_input('presione una tecla para continuar....')
	return True


def modificar_registro():
	#imprimir_listado()
	print "------Modificar Cliente--------"
	print ''
	pos = int(raw_input('Ingrese el regisitro que desea modificar: '))
	if pos>cant_registros:
		print 'Error: el numero de registro supera la cantidad permitida. [Cantidad MAX:',cant_registros,']'
		raw_input('presione una tecla para continuar....')
		return False
	cliente = leer_registro(pos)
	if cliente.estado != 1 :
		print "El registro esta vacio:"
		print "Ingrese a la opcion de [Agregar Cliente] del menu principal para darlo de alta"
		raw_input('presione una tecla para continuar....')
		return False
	imprimir_registro(cliente)
	print '-------------------------------'
	print "Ingrese los datos del cliente:"
	cliente.estado = str(cliente.estado)
	cliente.posicion = (str(cliente.posicion)).rjust(long_posicion,'0')
	cliente.apellido = (raw_input('Apellido: ')).ljust(long_apellido,' ')
	cliente.nombre = (raw_input('Nombre: ')).ljust(long_nombre,' ')
	cliente.codigo = (raw_input('Codigo: (numero de 5 digitos): ')).rjust(long_codigo,'0')
	if guardar_registro(cliente):
		print "Registro modificado existosamente"
	else:
		print "Error al intentar guardar el registro "
	raw_input('presione una tecla para continuar....')
	return True

def borrar_registro():
	#imprimir_listado()
	print "------Borrar Cliente--------"
	print ''
	
	pos = int(raw_input('Ingrese el regisitro que desea modificar: '))
	if pos>cant_registros:
		print 'Error: el numero de registro supera la cantidad permitida. [Cantidad MAX:',cant_registros,']'
		raw_input('presione una tecla para continuar....')
		return False
	cliente = leer_registro(pos)
	if cliente.estado == 0:
		print 'El registro esta vacio, no hay nada que borrar'
		raw_input('presione una tecla para continuar....')
	elif cliente.estado == 2:
		imprimir_registro(cliente)
		print "El cliente ya fue borrado, si desea recuperarlo ingrese a la opcion de [Recuperar Cliente]"
	else:
		imprimir_registro(cliente)
		remplazar = "a"
		while remplazar != "s": 
			remplazar = raw_input('Esta seguro que desea BORRAR el registro [n/s]: ')
			remplazar = remplazar.lower()
			if remplazar == "n":
				return False
		cliente.estado = '2'
		cliente.posicion = (str(cliente.posicion)).rjust(long_posicion,'0')
		guardar_registro(cliente)
		print '[borrando....]'
		print 'Registro borrado exitosamente'
	raw_input('presione una tecla para continuar....')
	return False


def generar_archivo():
	# comprobar si existe el archivo
	cantidad_registros = cant_registros
	if os.path.exists(nombre_archivo):
		print "El archivo de Clientes ya existe"
		remplazar = "a"
		while remplazar != "s": 
			remplazar = raw_input('Desea remplazar el archivo. Cuidado! Esto genera la perdida de todos los datos. [n/s]: ')
			remplazar = remplazar.lower()
			if remplazar == "n":
				return False		
	# generar el archivo nuevo
	print "Generar Archivo de Agenda - (agenda_uno.dat)"
	print "--------------------------------------------"
	archivo = open(nombre_archivo,'wb')
	
	for i in range(cantidad_registros):
		registro_libre = "0"+ str(i).rjust(long_posicion,'0')
		registro_libre = registro_libre.ljust(long_registro,' ')
		archivo.write(registro_libre)
	archivo.close()
	print "Agenda vacia generada existosamente"
	raw_input('presione una tecla para continuar....')
	return True


#PRINCIPAl
if __name__ == '__main__':
	menu = {}
	menu['0']="Listar Clientes" , listar_registros
	menu['1']="Agregar Cliente" , agregar_registro
	menu['2']="Modificar Cliente" , modificar_registro
	menu['3']="Borrar Cliente", borrar_registro
	menu['4']="Generar Archivo" , generar_archivo
	menu['5']= "Salir" , exit
	mensaje = "\n \n"
	while True: 
		os.system ("clear")
		print "--------AGENDA-----------"
		opciones=menu.keys()
		opciones.sort()
		for i in opciones: 
			print i , ':' , menu[i][0]

		print mensaje
		seleccion = raw_input('Ingrese la Opcion: ')
		
		if seleccion in opciones:
			os.system("clear")
			menu[seleccion][1]()
		else:
			mensaje = '\n Error: OPCION INCORRECTA \n'